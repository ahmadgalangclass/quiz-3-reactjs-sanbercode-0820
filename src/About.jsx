import React from "react";

const About = () => {
  return (
    <div>
      <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <ul>
        <li>
          <b>Nama : </b>Ahmad Galang Satria
        </li>
        <li>
          <b>Email : </b>ahmadgalangclass@gmail.com
        </li>
        <li>
          <b>Sistem Operasi yang digunakan : </b>Windows 10
        </li>
        <li>
          <b>Akun Gitlab : </b>ahmadgalangclass
        </li>
        <li>
          <b>Akun Telegram : </b>@gyozpada
        </li>
      </ul>
    </div>
  );
};

export default About;
